$('.dropify').dropify(); // init dropify

// init dialog
function initDialog(dialog) {
    var selectedDialog = document.getElementById(dialog);
    if (!selectedDialog.showModal) {
        dialogPolyfill.registerDialog(selectedDialog);
    }
    selectedDialog.querySelector('.close').addEventListener('click', function () {
        selectedDialog.close();
        selectedDialog.parentNode.removeChild(selectedDialog);
    });
    return selectedDialog;
};

// modular dialog
function showDialog(name, title, content) {
    dialogStorage = document.getElementById('dialogs');
    const newDialog = document.createElement('dialog');
    newDialog.className = 'mdl-dialog';
    newDialog.id = name;
    newDialog.innerHTML = `<h4 id="dialog-title" class="mdl-dialog__title">${title}</h4>
<div class="mdl-dialog__content">
    <p id="dialog-content">${content}</p>
</div>
<div class="mdl-dialog__actions">
    <button type="button" class="mdl-button close">Close</button>
</div>`;
    dialogStorage.appendChild(newDialog);
    dialog = initDialog(name);
    dialog.showModal();
};

// show stats because they need to be showed for showoff purposes
function displayStats() {
    function serviceDown() {
        // showDialog('serviceDown', 'Service is down.', 'RLME seems to be down at the moment. Please check out the status page, accessible at the bottom-right corner of this window for more information.');
    };
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://api.ratelimited.me/stats', true);
    xhr.onload = function () {
        response = JSON.parse(xhr.responseText);
        users = response['users']['total'];
        files = response['files'];
        document.getElementById('stats-users').innerHTML = `Users: ${users}`;
        document.getElementById('stats-files').innerHTML = `Files: ${files}`;
        document.getElementById('stats').classList.remove('hidden');
    };
    xhr.onerror = serviceDown();
    xhr.send();
};

// form work
form = document.getElementById('form');
form.onsubmit = function (event) {
    event.preventDefault();
    url = `https://api.ratelimited.me/upload/pomf?key=${localStorage.apikey}`;
    bar = document.getElementById('p1');
    bar.classList.remove('hidden');
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.upload.onprogress = function (progress) {
        if (progress.lengthComputable) {
            bar.MaterialProgress.setProgress((progress.loaded / progress.total) * 100);
        };
    };
    xhr.onload = function () {
        if (xhr.status === 200) {
            response = JSON.parse(xhr.responseText);
            filename = response['files'][0]['url'];
            fileURL = `https://${localStorage.domain}/${filename}`;
            showDialog('success', 'File uploaded!', `<a href="${fileURL}"><code>${fileURL}</code></a>`);
            bar.MaterialProgress.setProgress(0);
            bar.classList.add('hidden');
        } else {
            showDialog('failure', 'Upload failure.', `<code>${xhr.responseText}</code>`);
            bar.MaterialProgress.setProgress(0);
            bar.classList.add('hidden');
        }
    };
    let formdata = new FormData(form);
    xhr.send(formdata);
};

displayStats(); // display the stats