function loadSettingsToDOM() {
    document.getElementById('apikey').value = localStorage.apikey;
    document.getElementById('domain').value = localStorage.domain;
};

function saveSettings() {
    localStorage.apikey = document.getElementById('apikey').value;
    localStorage.domain = document.getElementById('domain').value;
};

loadSettingsToDOM();