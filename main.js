const electron = require('electron');
const path = require('path');
const url = require('url');
const {
    autoUpdater
} = require('electron-updater')
const fs = require('fs');
const Store = require('./store.js');

process.env.NODE_ENV = 'development';

const store = new Store({
    configName: 'user-preferences',
    defaults: {
        token: 'token'
    }
});

const {
    app,
    BrowserWindow,
    Menu,
    ipcMain
} = electron;

let mainWindow;
let uploadFile;
let settings;

app.on('ready', function () {
    autoUpdater.checkForUpdatesAndNotify()
    mainWindow = new BrowserWindow({
        width: 550,
        height: 550,
        minWidth: 550,
        minHeight: 550,
        title: 'RATELIMITED.ME'
    })
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'mainWindow.html'),
        protocol: 'file:',
        slashes: true
    }));
    mainWindow.on('closed', function () {
        app.quit();
    });

    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
    Menu.setApplicationMenu(mainMenu);
});

function createsettings() {
    settings = new BrowserWindow({
        width: 346,
        height: 257,
        minWidth: 346,
        minHeight: 257,
        maxWidth: 346,
        maxHeight: 257,
        title: 'Settings'
    });
    settings.loadURL(url.format({
        pathname: path.join(__dirname, 'settings.html'),
        protocol: 'file:',
        slashes: true
    }));
    settings.on('close', function () {
        settings = null;
    });
}

ipcMain.on('file:upload', function (e, file) {});

ipcMain.on('settings:setting', function (e, token) {});

const mainMenuTemplate = [{
    label: 'Settings',
    click() {
        createsettings();
    }
}];

if (process.platform == 'darwin') {
    mainMenuTemplate.unshift({});
}

if (process.env.NODE_ENV !== 'production') {
    mainMenuTemplate.push({
        label: 'Developer Tools',
        submenu: [{
                role: 'reload'
            },
            {
                label: 'Toggle DevTools',
                accelerator: process.platform == 'darwin' ? 'Command+I' : 'Ctrl+I',
                click(item, focusedWindow) {
                    focusedWindow.toggleDevTools();
                }
            }
        ]
    });
}